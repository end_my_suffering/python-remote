import subprocess, pickle, hmac, json
from threading import Thread
from base64 import b85encode

def run_unprivilleged(challenge, response):
    proc = subprocess.run(['/usr/bin/env', 'python3', 'self-test_unprivileged.py', challenge], capture_output=True, check=True)
    unprivilleged_response = pickle.loads(proc.stdout)
    response.update(unprivilleged_response)
    return True

def lshw_check(challenge, response):
    proc = subprocess.run(['lshw', '-notime', '-quiet', '-numeric', '-C', 'memory,processor,display,multimedia,communication,system,generic,bridge'], capture_output=True, check=True)
    response['l'] = b85encode(hmac.digest(challenge.encode(), proc.stdout, 'SHA256')).decode()
    return True

def main():
    challenge = '4JjfKUAKlyhlumJozvL9V2HESpNZElj6'
    response = dict()
    
    unpri_thread = Thread(target=run_unprivilleged, kwargs={'challenge':challenge, 'response':response})
    unpri_thread.start()
    lshw_thread = Thread(target=lshw_check, kwargs={'challenge':challenge, 'response':response})
    lshw_thread.start()
    
    unpri_thread.join()
    lshw_thread.join()
    
#     print(response)
    json_response = json.dumps(response)
    print(json_response)
    print(f"len(json_response)={len(json_response)}")

if __name__ == "__main__":
    main()