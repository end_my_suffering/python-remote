from tkinter import Tk, Label
from threading import Thread
from pathlib import Path
import subprocess, os

class desktop_alert:
    def __init__(self):
        dir_path = Path(__file__).resolve().parent
        self.audio_alert_path = dir_path / 'res' / 'ussr_anthem_1977_wikipedia.oga'
        
        cp_to_tmp_thread = Thread(target = self.copy_audio_file_to_tmp)
        cp_to_tmp_thread.start()
        
        cp_to_tmp_thread.join()
        
        self.demote(1000,1000)
        os.environ['XAUTHORITY'] = '/home/adam/.Xauthority'
        os.environ['DISPLAY'] = ':0.0'
        os.environ['USER'] = 'adam'
        os.environ['LOGNAME'] = 'adam'
        os.environ['HOME'] = '/home/adam'
        
        audio_alert_thread = Thread(target = self.play_audio_alert)
        audio_alert_thread.start()
        
        window = Tk()
        self.window = window
        self.time_left = 10
        self.window.lift()
        self.window.attributes('-topmost', True)
        self.window.title("### 1606 WARNING ###")
        self.window.geometry('800x600')
        self.window['bg'] = 'white'

        self.title_lbl = Label(self.window, text="!!!! DISK ERASE NOW !!!!")
        self.title_lbl.config(font=("Courier", 32))
        self.title_lbl.place(relx=.5, rely=.5, anchor="center")

        self.est_lbl = Label(self.window, text="in 10 sec")
        self.est_lbl.config(font=("Courier", 32))
        self.est_lbl.place(relx=.5, rely=.6, anchor="center")

        self.window.after(1000, self.count_down)
        self.window.after(300, self.flash_screen)
        
        self.window.mainloop()

    def flash_screen(self):
        if self.time_left > 0:
            if self.window['bg'] == 'yellow':
                self.window['bg'] = 'white'
            elif self.window['bg'] == 'white':
                self.window['bg'] = 'red'
            elif self.window['bg'] == 'red':
                self.window['bg'] = 'yellow'
            self.window.after(300, self.flash_screen)
            
        return True

    def count_down(self):        
        if self.time_left > 0:
            self.time_left = self.time_left-1
            self.est_lbl.config(text=f"in {self.time_left} sec")
            self.window.after(1000, self.count_down)
        elif self.time_left == 0:
            self.window['bg'] = 'red'
            self.window.title("USSR lyrics")
            self.title_lbl.config(text="Lyrics of our glorious USSR Anthem")
            self.title_lbl.config(font=("Courier", 22))
            self.est_lbl.config(text="soyuz nerushimy respublik svobodnykh,\nAn unbreakable union of free republics,\n\nSplotila navěki Velikaja Rus!\nThe Great Russia has welded forever to stand.")
            self.est_lbl.config(font=("Courier", 16))
            self.est_lbl.place(relx=.5, rely=.7, anchor="center")
        
    def play_audio_alert(self):
        subprocess.run(['mpv', '--no-terminal', '--no-config', '--volume=80', '--loop=yes', '/tmp/ussr_anthem_1977_wikipedia.oga'])
        return True
    
    def copy_audio_file_to_tmp(self):
        subprocess.run(['cp', str(self.audio_alert_path), '/tmp/ussr_anthem_1977_wikipedia.oga'], check=False)
        return True
    
    def demote(self, uid, gid):
        os.setgid(gid)
        os.setuid(uid)
        return True